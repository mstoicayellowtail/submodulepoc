FROM mcr.microsoft.com/dotnet/core/sdk:2.2 AS build-env
WORKDIR /app

# Copy csproj and restore as distinct layers
COPY oauthapimicrosoft/OAuthAPIMicrosoft/OAuthAPIMicrosoft.csproj oauthapimicrosoft/OAuthAPIMicrosoft/
COPY oauthapimicrosoft/OAuthAPIMicrosoft/NuGet.config oauthapimicrosoft/OAuthAPIMicrosoft/
RUN dotnet restore oauthapimicrosoft/OAuthAPIMicrosoft/OAuthAPIMicrosoft.csproj --configfile oauthapimicrosoft/OAuthAPIMicrosoft/NuGet.config
#RUN dotnet list OAuthAPIMicrosoft/OAuthAPIMicrosoft.csproj package

# Copy everything else and build
COPY ./ ./
RUN find -type d -name bin -prune -exec rm -rf {} \; && find -type d -name obj -prune -exec rm -rf {} \;
RUN dotnet build oauthapimicrosoft/OAuthAPIMicrosoft/OAuthAPIMicrosoft.csproj

# Build runtime image


